# README #

This README documents the steps that are necessary to get this web application up and running

### How do I get set up? ###

* You will require the [TurnKey Linux LAMP VM](http://www.turnkeylinux.org/lampstack)
* If using [VirtualBox](https://www.virtualbox.org/), you will need to change the network adaptor from NAT to Bridged Adaptor, and enable the extended processor feature PAE/NX
* Set the root and MySQL passwords for the LAMP appliance to "tangam" when prompted
* Upload the setup.bash script from this repository to the LAMP appliance /root directory, then execute it
* Open a local web browser and point it to the IP address of the LAMP appliance
* Login credentials for the web application is "myfamily" for both user name and password

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact