<?php
//=========================================================
// Project:		Tangam Test
// Author:		Chris Capobianco <ccapo.astro@gmail.com>
// Date:		2014-08-01
//=========================================================
require_once("include/db.php");
$logonSuccess = false;

// Ensure site is using encryption
if( empty($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] !== "on" )
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

// Verify user's credentials
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $logonSuccess = (clientDB::getInstance()->verify_credentials($_POST['user'], $_POST['password']));
    if($logonSuccess == true) {
        session_start();
        $_SESSION['user'] = $_POST['user'];
        header('Location: manager.php');
        exit;
    }
}

if(array_key_exists("user", $_SESSION)) {
	header('Location: manager.php');
	exit;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Shopping Manager</title>
		<link rel='stylesheet' href='css/jquery-ui/jquery-ui.min.css'>
		<link rel='stylesheet' href='css/site.css'>
		<script type='text/javascript' src='js/jquery/jquery.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui/jquery-ui.min.js'></script>
		<script type='text/javascript' src='js/manager.js'></script>
	</head>
	<body>
	<?php include("include/header.php"); ?>

	<div id="main">

		<h1>Shopping Manager</h1>

		<form name="logon" action="index.php" method="POST">
			<label for="name">Username:</label>
			<input type="text" name="user"><br/>
			<label for="password">Password:</label>
			<input type="password" name="password"><br/>
			<input type="submit" value="Submit">
            <?php
            if($_SERVER['REQUEST_METHOD'] == "POST") {
                if(!$logonSuccess) {
                    echo "<p style='font-weight: bold;'>Invalid username and/or password</p>";
				}
            }
            ?>
		</form>

		<?php include("include/footer.php"); ?>

	</div>

	</body>

</html>
