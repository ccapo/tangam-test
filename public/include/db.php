<?php
//=========================================================
// Project:		Tangam Test
// Author:		Chris Capobianco <ccapo.astro@gmail.com>
// Date:		2014-08-01
//=========================================================
require_once("password.php");

class clientDB extends mysqli {

    // Single instance of self shared among all instances
    private static $instance = null;
    // DB connection config vars
    private $user = "tangam";
    private $pass = "tangam";
    private $dbName = "tangam";
    private $dbHost = "localhost";
    private $con = null;

    // This method must be static, and must return an instance of the object if the object
    // does not already exist.
    public static function getInstance() {
        if(!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    // The clone and wakeup methods prevents external instantiation of copies of the Singleton class,
    // thus eliminating the possibility of duplicate objects.
    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Deserializing is not allowed.', E_USER_ERROR);
    }

    // private constructor
    private function __construct() {
        parent::__construct($this->dbHost, $this->user, $this->pass, $this->dbName);
        if(mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        parent::set_charset('utf-8');
    }

    public function verify_credentials($user, $password) {
        $user = $this->real_escape_string($user);
        $password = $this->real_escape_string($password);
		$result = $this->query("SELECT password FROM users WHERE user = '" . $user . "' limit 1");
		$hash = $result->fetch_array(MYSQLI_NUM);
		$result->free();
		return password_verify($password, $hash[0]);
    }

    public function get_shopping_data() {
		return $this->query("SELECT * FROM shopping");
    }

    public function create_entry($name, $quantity, $uom) {
        $name = $this->real_escape_string($name);
        $quantity = $this->real_escape_string($quantity);
        $uom = $this->real_escape_string($uom);
        return $this->query("INSERT INTO shopping (name, quantity, uom) VALUES ('" . $name . "', '" . $quantity . "', '" . $uom . "')");
    }

    public function edit_entry($id, $name, $quantity, $uom) {
        $id = $this->real_escape_string($id);
        $name = $this->real_escape_string($name);
        $quantity = $this->real_escape_string($quantity);
        $uom = $this->real_escape_string($uom);
        return $this->query("UPDATE shopping SET name = '" . $name . "', quantity = '" . $quantity . "', uom = '" . $uom . "' WHERE id = " . $id);
    }

    public function delete_entries($idarray) {
		// Loop Over $_POST Array Values
		$result = true;
		foreach( $idarray as $postitem ) {

			// Escape Variables For Security
			$id = $this->real_escape_string( $postitem );

			// Execute Query
			if( !$this->query("DELETE FROM shopping WHERE id = " . $id) ) $result = false;

		}

		return $result;
    }
}
?>
