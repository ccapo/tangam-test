<?php
//=========================================================
// Project:		Tangam Test
// Author:		Chris Capobianco <ccapo.astro@gmail.com>
// Date:		2014-08-01
//=========================================================
	session_start();
    session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(),'',0,'/');
    session_regenerate_id(true);
?>
