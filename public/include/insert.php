<?php
//=========================================================
// Project:		Tangam Test
// Author:		Chris Capobianco <ccapo.astro@gmail.com>
// Date:		2014-08-01
//=========================================================
	require_once("db.php");

	// Execute Query
	if( clientDB::getInstance()->create_entry($_POST['name'], $_POST['quantity'], $_POST['uom']) ) {

		// Reponse Statement
		echo "Success: Record Was Created";

	} else {

		// Reponse Statement
		echo "Error: Record Was Not Created!";

	}
?>
