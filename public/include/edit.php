<?php
//=========================================================
// Project:		Tangam Test
// Author:		Chris Capobianco <ccapo.astro@gmail.com>
// Date:		2014-08-01
//=========================================================
	require_once("db.php");

	// Execute Query
	if( clientDB::getInstance()->edit_entry($_POST['id'], $_POST['name'], $_POST['quantity'], $_POST['uom']) ) {

		// Reponse Statement
		echo "Success: Record Was Updated";

	} else {

		// Reponse Statement
		echo "Error: Record Was Not Updated!";

	}
?>
