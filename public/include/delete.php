<?php
//=========================================================
// Project:		Tangam Test
// Author:		Chris Capobianco <ccapo.astro@gmail.com>
// Date:		2014-08-01
//=========================================================
	require_once("db.php");

	// Execute Query
	if( clientDB::getInstance()->delete_entries( $_POST['id'] ) ) {

		// Reponse Statement
		echo "Success: Record(s) Were Removed";

	} else {

		// Reponse Statement
		echo "Error: Record(s) Were Not Removed!";

	}
?>
