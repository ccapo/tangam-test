//=========================================================
// Project:		Tangam Test
// Author:		Chris Capobianco <ccapo.astro@gmail.com>
// Date:		2014-08-01
//=========================================================
$(document).ready(function() {

	$( "input[type=button],input[type=submit]" ).button();

	$( "#create-dialog" ).dialog({
		autoOpen: false,
		resizable: false,
		width:400,
		height:250,
		modal: true,
		buttons: {
			OK: function() {

				var data = {};
				data.name = $( "#new_name" ).val();
				data.quantity = $( "#new_quantity" ).val();
				data.uom = $( "#new_uom" ).val();

				//console.log( data );

				$.ajax({
					url: "include/insert.php",
					type: "POST",
					data: data,
					success: function( response ) {
						//alert( response );
						window.location = "manager.php";
					}
				});

				$( this ).dialog( "close" );

			},
			Cancel: function() {

				$( this ).dialog( "close" );

			}
		}
	});
	$( "#create-button" ).click(function() {
		$( "#create-dialog" ).dialog( "open" );
	});

	$( "#edit-dialog" ).dialog({
		autoOpen: false,
		resizable: false,
		width:400,
		height:250,
		modal: true,
		buttons: {
			OK: function() {

				var data = {};
				$( ".selected:checked" ).each(function() {

					data.id = parseInt( $( this ).parent().next().text() );
          
				});
				data.name = $( "#edit_name" ).val();
				data.quantity = $( "#edit_quantity" ).val();
				data.uom = $( "#edit_uom" ).val();

				//console.log( data );

				$.ajax({
					url: "include/edit.php",
					type: "POST",
					data: data,
					success: function( response ) {
						//alert( response );
						window.location = "manager.php";
					}
				});

				$( this ).dialog( "close" );

			},
			Cancel: function() {

				$( this ).dialog( "close" );

			}
		}
	});
	$( "#edit-button" ).click(function() {

		if( $('.selected:checked').length == 1 ) {

			var data = {};
			$( ".selected:checked" ).each(function() {

				data.id = parseInt( $( this ).parent().next().text() );
				data.name = $( this ).parent().next().next().text();
				data.quantity = $( this ).parent().next().next().next().text();
				data.uom = $( this ).parent().next().next().next().next().text();
          
			});

			$( "#edit_name" ).val( data.name );
			$( "#edit_quantity" ).val( data.quantity );
			$( "#edit_uom" ).val( data.uom );

			$( "#edit-dialog" ).dialog( "open" );

		} else {

			alert( "Please Select A Single Item To Edit" );

		}

	});

	$( "#delete-dialog" ).dialog({
		autoOpen: false,
		resizable: false,
		width:400,
		height:225,
		modal: true,
		buttons: {
			OK: function() {

				var ids = [];
				$( ".selected:checked" ).each(function() {

					ids.push( parseInt( $( this ).parent().next().text() ) );
          
				});

				var data = {};
				data.id = ids

				//console.log( data );

				$.ajax({
					url: "include/delete.php",
					type: "POST",
					data: data,
					success: function( response ) {
						//alert( response );
						window.location = "manager.php";
					}
				});

				$( this ).dialog( "close" );

			},
			Cancel: function() {

				$( this ).dialog( "close" );

			}
		}
	});
	$( "#delete-button" ).click(function() {

		if( $('.selected:checked').length > 0 ) {

			$( "#delete-dialog" ).dialog( "open" );

		} else {

			alert( "Please Select An Item To Delete" );

		}

	});

	$( "#logout-dialog" ).dialog({
		autoOpen: false,
		resizable: false,
		width:400,
		height:225,
		modal: true,
		buttons: {
			OK: function() {

				$.ajax({
					url: "include/logout.php",
					type: "POST",
					success: function( data ) {
						window.location = "index.php";
					}
				});

				$( this ).dialog( "close" );

			},
			Cancel: function() {

				$( this ).dialog( "close" );

			}
		}
	});
	$( "#logout-button" ).click(function() {

		$( "#logout-dialog" ).dialog( "open" );

	});

	$('#select-all').click(function(event) { 

		if( this.checked ) {

			$('.selected').each(function() {

				this.checked = true;     
          
			});

		} else {

			$('.selected').each(function() {

				this.checked = false;    

			});  
       
		}
	});
});
