<!--
Project:	Tangam Test
Author:		Chris Capobianco <ccapo.astro@gmail.com>
Date:		2014-08-01
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Shopping Manager</title>
		<link rel='stylesheet' href='css/jquery-ui/jquery-ui.min.css'>
		<link rel='stylesheet' href='css/site.css'>
		<script type='text/javascript' src='js/jquery/jquery.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui/jquery-ui.min.js'></script>
		<script type='text/javascript' src='js/manager.js'></script>
	</head>
	<body>
	<?php
		include("include/header.php");

		session_start();
		if (!array_key_exists("user", $_SESSION)) {
			header('Location: index.php');
			exit;
		}
	?>

	<div id="main">

		<h1>Shopping Manager</h1>

		<input type="button" id="create-button" style="float:left" title="Create New Client" value="Create"/>
		<input type="button" id="edit-button" style="float:left" title="Edit Selected Client" value="Edit"/>
		<input type="button" id="delete-button" style="float:left" title="Delete Selected Client" value="Delete"/>
		<input type="button" id="logout-button" style="float:right" title="Logout of Instance Manager" value="Logout"/>

		<?php
			require_once("include/db.php");
			$result = clientDB::getInstance()->get_shopping_data();

			echo "<table class='reference'>
				<tr>
				<th><input type='checkbox' id='select-all'/></th>
				<th>ID</th>
				<th>Name</th>
				<th>Quantity</th>
				<th>Units</th>
				</tr>";

			while($row = $result->fetch_array(MYSQLI_ASSOC)) {
				echo "<tr>";
				echo "<td><input type='checkbox' class='selected'/></td>";
				echo "<td>" . $row['id'] . "</td>";
				echo "<td>" . $row['name'] . "</td>";
				echo "<td>" . $row['quantity'] . "</td>";
				echo "<td>" . $row['uom'] . "</td>";
				echo "</tr>";
			}

			echo "</table>";

  			$result->free();
		?>

		<?php include("include/footer.php"); ?>

	</div>

	<div id="create-dialog" title="Create Shopping Item">

		<table class="dialog">
			<tbody>
				<tr>
					<td>
						Name:
					</td>
					<td>
						<input type="text" id="new_name"/>
					</td>
				</tr>
				<tr>
					<td>
						Quantity:
					</td>
					<td>
						<input type="text" id="new_quantity"/>
					</td>
				</tr>
				<tr>
					<td>
						Units:
					</td>
					<td>
						<input type="text" id="new_uom"/>
					</td>
				</tr>
			</tbody>
		</table>

	</div>

	<div id="edit-dialog" title="Edit Shopping Item">

		<table class="dialog">
			<tbody>
				<tr>
					<td>
						Name:
					</td>
					<td>
						<input type="text" id="edit_name"/>
					</td>
				</tr>
				<tr>
					<td>
						Quantity:
					</td>
					<td>
						<input type="text" id="edit_quantity"/>
					</td>
				</tr>
				<tr>
					<td>
						Units:
					</td>
					<td>
						<input type="text" id="edit_uom"/>
					</td>
				</tr>
			</tbody>
		</table>

	</div>

	<div id="delete-dialog" title="Delete Shopping Items">
		<p>Delete the selected shopping items?</p>
	</div>

	<div id="logout-dialog" title="Logout">
		<p>Are you sure you want to logout?</p>
	</div>

	</body>
</html>
