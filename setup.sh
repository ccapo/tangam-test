#!/bin/bash

# Create DB User, new DB, new table for users, new table for shopping list
#cat << EOF > database.sql
#CREATE USER 'tangam'@'localhost' IDENTIFIED BY PASSWORD '*3DD54E27E7AF6B33EE4FCF5A7F36D2AB054BB382';
#GRANT ALL PRIVILEGES ON * . * TO 'tangam'@'localhost';
#CREATE DATABASE tangam;
#USE tangam;
#CREATE TABLE users(id int not null auto_increment, primary key(id), user varchar(255) not null, password varchar(255) not null);
#INSERT INTO users (user, password) values ("myfamily", "$2y$10$BNPzaWEup.a29mVcNt1GtOXD19pd74WAgDwaWKklhCzLTWMrY5sZm");
#CREATE TABLE shopping(id int not null auto_increment, primary key(id), name varchar(255) not null, quantity int not null, uom varchar(255) not null);
#EOF
mysql -u root -ptangam < database.sql
rm database.sql

# Clone Git Repository of Web Application
cd /var
git clone https://ccapo@bitbucket.org/ccapo/tangam-test.git tangam-test
rm -fr www
ln -s tangam-test/public www

# Replace AllowOverride None with All in Apache security
cat << EOF2 >> /etc/apache2/conf.d/security
<Directory />
AllowOverride All
Order Deny,Allow
Deny from all
</Directory>
EOF2

# Add Virtual Host for Web Application
cat << EOF3 > /etc/apache2/sites-available/tangam-test.conf
<VirtualHost *:80>
DocumentRoot /var/www
ServerName http://www.tangam-test.dev
ServerAlias tangam-test.dev
<Directory /var/www>
Options FollowSymlinks
Order allow,deny
AllowOverride All
Allow from All
Require all granted
</Directory>
</VirtualHost>
EOF3

# Update Apache configuration and Restart
a2enmod rewrite
cd /etc/apache2/sites-available
a2ensite tangam-test.conf
service apache2 restart
